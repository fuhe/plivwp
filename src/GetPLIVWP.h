// This is -*- c++ -*-
#ifndef ANP_PREPPLIVWP_H
#define ANP_PREPPLIVWP_H

/**********************************************************************************
 * @Package: PhysicsAnpLight
 * @Class  : GetPLIVWP
 * @Author : Fudong He
 *
 * @Brief  :
 * 
 * Add PLIV working point with given cuts 
 * 
 **********************************************************************************/

#include <vector>
#include <iostream>

namespace PLT 
{
  enum IsoWP {
    PLIVTight,
    PLIVVeryTight,
  }; 

  //
  // Helper class for getting the PLIV cuts
  //
  class GetPLIVWP
  {
  public:
    GetPLIVWP();
    
    virtual ~GetPLIVWP();
    
    bool passPLIVMuonWorkingPoint(double pT, double lep_ptbin, double pliv, IsoWP wp);
   
    bool passPLIVElecWorkingPoint(double pT, double lep_ptbin, double lep_eta, double pliv, IsoWP wp);
      
    bool passPLIVMuonWorkingPoint(double pT, double pliv, IsoWP wp);
    bool passPLIVElecWorkingPoint(double pT, double lep_eta, double pliv, IsoWP wp);
   
    bool passLepPLIVCut(double pT, double pliv, std::vector<double> left_cuts, std::vector<double> right_cuts);
    
    bool passLepPLIVCutBin(double pT, double ptbin, double pliv, std::vector<double> left_cuts, std::vector<double> right_cuts);

  private:

    std::vector<double>          m_PtBins;
    
  };
}

#endif
