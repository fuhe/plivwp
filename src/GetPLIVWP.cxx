#include "GetPLIVWP.h"
using namespace std;

//==============================================================================
PLT::GetPLIVWP::GetPLIVWP():
  m_PtBins{10e3, 15e3, 20e3, 25e3, 32e3, 43e3}
{
}

//==============================================================================
PLT::GetPLIVWP::~GetPLIVWP()
{
}

//==============================================================================
bool PLT::GetPLIVWP::passPLIVMuonWorkingPoint(double lep_pt, double lep_ptbin, double lep_pliv, PLT::IsoWP wp)
{
  //
  // Prepare the PLIV working points cuts values in different pT bin edges 
  //
  vector<double> PLIVBinLeftCut(m_PtBins.size()), PLIVBinRightCut(m_PtBins.size());
  if(wp == PLT::PLIVTight) {
    //
    // prompt efficiency at each pT bin edges = 0.42, 0.50, 0.59, 0.73, 0.85, 0.95 
    //
    PLIVBinLeftCut  = vector<double> {-0.7631, -0.872, -0.9068, -0.887, -0.8445, -0.6734};
    PLIVBinRightCut = vector<double> {-0.8224, -0.8781, -0.8641, -0.8236, -0.7272, -0.6734};
  }
  else if(wp == PLT::PLIVVeryTight) {
    //
    // prompt efficiency at each pT bin edges = 0.38, 0.47, 0.53, 0.58, 0.72, 0.91
    // 
    PLIVBinLeftCut  = vector<double> {-0.7908, -0.8876, -0.9308, -0.9522, -0.9449, -0.8469};
    PLIVBinRightCut = vector<double> {-0.8374, -0.9054, -0.9362, -0.9272, -0.8564, -0.8469};

  }

  return passLepPLIVCutBin(lep_pt, lep_ptbin, lep_pliv, PLIVBinLeftCut, PLIVBinRightCut);

}

//==============================================================================
bool PLT::GetPLIVWP::passPLIVElecWorkingPoint(double lep_pt, double lep_ptbin, double lep_eta, double lep_pliv, PLT::IsoWP wp)
{
  //
  // Prepare the PLIV working points cuts values in different pT bin edges 
  //
  vector<double> PLIVBinLeftCut(m_PtBins.size()), PLIVBinRightCut(m_PtBins.size());

  if(abs(lep_eta) < 1.37) {
    if(wp == PLT::PLIVTight) {
      //
      // prompt efficiency at each pT bin edges = .42, .53, .63, .71, .85, .95
      //
      PLIVBinLeftCut  = vector<double> {-0.7842, -0.8742, -0.9056, -0.9169, -0.8626, -0.7039};
      PLIVBinRightCut = vector<double> {-0.8326, -0.8819, -0.8966, -0.8498, -0.7733, -0.7039};
    }
    else if(wp == PLT::PLIVVeryTight) {
      //
      // prompt efficiency at each pT bin edges = .40, .48, .54, .60, .68, .85
      // 
      PLIVBinLeftCut  = vector<double> {-0.7984, -0.8997, -0.9397, -0.9561, -0.9632, -0.9359};
      PLIVBinRightCut = vector<double> {-0.8556, -0.92, -0.9408, -0.9518, -0.936, -0.9359};

    }
  }
  else {
    if(wp == PLT::PLIVTight) {
      //
      // prompt efficiency at each pT bin edges = .42, .60, .72, .77, .80, .83 
      // 
      PLIVBinLeftCut  = vector<double> {-0.6885, -0.7472, -0.7619, -0.8059, -0.8605, -0.914};
      PLIVBinRightCut = vector<double> {-0.694, -0.7162, -0.7789, -0.8309, -0.9088, -0.914};
    }
    else if(wp == PLT::PLIVVeryTight) {
      //
      // prompt efficiency at each pT bin edges = .33, .49, .61, .66, .71, .78 -- based on raw pT
      // 
      PLIVBinLeftCut  = vector<double> {-0.8353, -0.8846, -0.8987, -0.9223, -0.9464, -0.9698};
      PLIVBinRightCut = vector<double> {-0.8114, -0.8571, -0.8995, -0.9241, -0.9545, -0.9698};
    }
  }

  return passLepPLIVCutBin(lep_pt, lep_ptbin, lep_pliv, PLIVBinLeftCut, PLIVBinRightCut);
}


//==============================================================================
bool PLT::GetPLIVWP::passPLIVMuonWorkingPoint(double lep_pt, double lep_pliv, PLT::IsoWP wp)
{
  //
  // Prepare the PLIV working points cuts values in different pT bin edges 
  //
  vector<double> PLIVBinLeftCut(m_PtBins.size()), PLIVBinRightCut(m_PtBins.size());
  if(wp == PLT::PLIVTight) {
    //
    // prompt efficiency at each pT bin edges = 0.42, 0.50, 0.59, 0.73, 0.85, 0.95 
    //
    PLIVBinLeftCut  = vector<double> {-0.7631, -0.872, -0.9068, -0.887, -0.8445, -0.6734};
    PLIVBinRightCut = vector<double> {-0.8224, -0.8781, -0.8641, -0.8236, -0.7272, -0.6734};
  }
  else if(wp == PLT::PLIVVeryTight) {
    //
    // prompt efficiency at each pT bin edges = 0.38, 0.47, 0.53, 0.58, 0.72, 0.91
    // 
    PLIVBinLeftCut  = vector<double> {-0.7908, -0.8876, -0.9308, -0.9522, -0.9449, -0.8469};
    PLIVBinRightCut = vector<double> {-0.8374, -0.9054, -0.9362, -0.9272, -0.8564, -0.8469};

  }

  return passLepPLIVCut(lep_pt, lep_pliv, PLIVBinLeftCut, PLIVBinRightCut);

}

//==============================================================================
bool PLT::GetPLIVWP::passPLIVElecWorkingPoint(double lep_pt, double lep_eta, double lep_pliv, PLT::IsoWP wp)
{
  //
  // Prepare the PLIV working points cuts values in different pT bin edges 
  //
  vector<double> PLIVBinLeftCut(m_PtBins.size()), PLIVBinRightCut(m_PtBins.size());

  if(abs(lep_eta) < 1.37) {
    if(wp == PLT::PLIVTight) {
      //
      // prompt efficiency at each pT bin edges = .42, .53, .63, .71, .85, .95
      //
      PLIVBinLeftCut  = vector<double> {-0.7842, -0.8742, -0.9056, -0.9169, -0.8626, -0.7039};
      PLIVBinRightCut = vector<double> {-0.8326, -0.8819, -0.8966, -0.8498, -0.7733, -0.7039};
    }
    else if(wp == PLT::PLIVVeryTight) {
      //
      // prompt efficiency at each pT bin edges = .40, .48, .54, .60, .68, .85
      // 
      PLIVBinLeftCut  = vector<double> {-0.7984, -0.8997, -0.9397, -0.9561, -0.9632, -0.9359};
      PLIVBinRightCut = vector<double> {-0.8556, -0.92, -0.9408, -0.9518, -0.936, -0.9359};

    }
  }
  else {
      //
      // prompt efficiency at each pT bin edges = .42, .60, .72, .76, .81, .85 -- large stats.
      // 
      PLIVBinLeftCut  = vector<double> {-0.6885, -0.7472, -0.7619, -0.8059, -0.8605, -0.914};
      PLIVBinRightCut = vector<double> {-0.694, -0.7162, -0.7789, -0.8309, -0.9088, -0.914};
  }

  return passLepPLIVCut(lep_pt, lep_pliv, PLIVBinLeftCut, PLIVBinRightCut);
}

//==============================================================================
bool PLT::GetPLIVWP::passLepPLIVCut(double lep_pt, double lep_pliv, vector<double> left_cuts, vector<double> right_cuts)
{
  bool pass_pliv = false;
  //  
  // Iterate the first n-1 pT bins
  //  
  for(unsigned bin = 0; bin < m_PtBins.size() - 1; ++bin) {
    if(lep_pt > m_PtBins[bin]) {
      if(lep_pt < m_PtBins[bin+1]) {
        double k = (right_cuts[bin] - left_cuts[bin])/(m_PtBins[bin+1] - m_PtBins[bin]);
        double b = right_cuts[bin] - m_PtBins[bin+1]*k;
        
        if(lep_pliv < k*lep_pt + b) { pass_pliv = true;}
      }   
    }   
  }   

  // For the last pT bin
  if(!pass_pliv){ 
    if(lep_pt > m_PtBins[m_PtBins.size()-1]) {
      if(lep_pliv < right_cuts[m_PtBins.size()-1]) { pass_pliv = true;}
    }   
  }
  
  return pass_pliv;   
}
//==============================================================================
bool PLT::GetPLIVWP::passLepPLIVCutBin(double lep_pt, double lep_ptbin, double lep_pliv, vector<double> left_cuts, vector<double> right_cuts)
{
  bool pass_pliv = false;
  //  
  // Iterate the first n-1 pT bins
  // 
  // For the ptbin 0, refers to raw pT < 10 GeV
  if(lep_ptbin == 0) {
     if(lep_pliv < left_cuts[0]) { pass_pliv = true;}
  } 
  else if (lep_ptbin == m_PtBins.size()) {
    if(lep_pliv < right_cuts[m_PtBins.size()-1]) { pass_pliv = true;}
  }  
  else if(lep_ptbin < 0) {
    // always pass if there is no track jet near by
    return true;
  }
  else { 
    double k = (right_cuts[lep_ptbin-1] - left_cuts[lep_ptbin-1])/(m_PtBins[lep_ptbin] - m_PtBins[lep_ptbin-1]);
    double b = right_cuts[lep_ptbin-1] - m_PtBins[lep_ptbin]*k;

    if(lep_pliv < k*lep_pt + b) { pass_pliv = true;}
  }

  return pass_pliv;   
}
