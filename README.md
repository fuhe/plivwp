# Introduction to the PromptLeptonImprovedVeto working points
[IFF meeting](https://indico.cern.ch/event/956057/);\
[EGamma meeting](https://indico.cern.ch/event/978569/);\
[Muon Combined Performance meeting](https://indico.cern.ch/event/962542/)

# PromptLeptonImprovedVeto working points c++ class usage
## muon working point

```c++
m_PLIVWP_tool = PLT::GetPLIVWP();

pass_pliv_tight     = m_PLIVWP_tool.passPLIVMuonWorkingPoint(muon_pt, muon_ptbin, muon_pliv, PLT::PLIVTight);
pass_pliv_verytight = m_PLIVWP_tool.passPLIVMuonWorkingPoint(muon_pt, muon_ptbin, muon_pliv, PLT::PLIVVeryTight);
```
where the "muon_pliv" is the output of the "PromptLeptonImprovedVeto".
The "muon_ptbin" is the "PromptLeptonImprovedInput_MVAXBin".

- If the "PromptLeptonImprovedInput_MVAXBin" is not available in your framework, you can try:
```c++
m_PLIVWP_tool = PLT::GetPLIVWP();

pass_pliv_tight     = m_PLIVWP_tool.passPLIVMuonWorkingPoint(muon_pt, muon_pliv, PLT::PLIVTight);
pass_pliv_verytight = m_PLIVWP_tool.passPLIVMuonWorkingPoint(muon_pt, muon_pliv, PLT::PLIVVeryTight);
```

## electron working point
```c++
if(abs(elec_eta)<1.37) {
      // use some fun. to get the "PromptLeptonImprovedVetoBARR" for barral region electron
      elec_pliv = func(PromptLeptonImprovedVetoBARR);
          }
else {
      // use some fun. to get the "PromptLeptonImprovedVetoECAP" for endcap region electron
      elec_pliv = func(PromptLeptonImprovedVetoECAP);
          }
    
pass_pliv_tight     = m_PLIVWP_tool.passPLIVElecWorkingPoint(elec_pt, elec_ptbin, elec_eta, elec_pliv, PLT::PLIVTight);
pass_pliv_verytight = m_PLIVWP_tool.passPLIVElecWorkingPoint(elec_pt, elec_ptbin, elec_eta, elec_pliv, PLT::PLIVVeryTight);

```
elec_ptbin refers to "PromptLeptonImprovedInput_MVAXBin". 
If it is not available in your framework, you can use below instead:
```
pass_pliv_tight     = m_PLIVWP_tool.passPLIVElecWorkingPoint(elec_pt, elec_eta, elec_pliv, PLT::PLIVTight);
pass_pliv_verytight = m_PLIVWP_tool.passPLIVElecWorkingPoint(elec_pt, elec_eta, elec_pliv, PLT::PLIVVeryTight);
```

## About the "PromptLeptonImprovedInput_MVAXBin"
We recommand to use this variable for your working points. 
It based on the lepton raw pT, and should give you a smoother efficiency as function of pT.

But there should no large difference in performance between use or do not use "PromptLeptonImprovedInput_MVAXBin".

