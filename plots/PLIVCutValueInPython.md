# PromptLeptonImprovedVeto working points in python
## muon working point
pT refers to lepton pT in GeV, pTbin refers to "**PromptLeptonImprovedInput_MVAXBin**" and pliv refres to "**PromptLeptonImprovedVeto**".
- PLImprovedTight cut values:

![promptleptontight muon](plots/muon/PLIVWP_cuts_cut.png)
  ```py
# lepton pT [GeV]
  cut = -99.
  if pTbin <= 0: cut = -0.7631
  elif pTbin == 1: cut = -0.01186*pT + -0.6445
  elif pTbin == 2: cut = -0.00122*pT + -0.8537
  elif pTbin == 3: cut = 0.00854*pT + -1.0776
  elif pTbin == 4: cut = 0.00905714285714*pT + -1.11342857143
  elif pTbin == 5: cut = 0.0106636363636*pT + -1.18573636364
  else: cut = -0.6734 

  pass_pliv_tight = pliv < cut
  ```
  - PLImprovedVeryTight cut values:

  ```py
# lepton pT [GeV]
  cut = -99.
  if pTbin <= 0: cut = -0.7908
  elif pTbin == 1: cut = -0.00932*pT + -0.6976
  elif pTbin == 2: cut = -0.00356*pT + -0.8342
  elif pTbin == 3: cut = -0.00108*pT + -0.9092
  elif pTbin == 4: cut = 0.00357142857143*pT + -1.04148571429
  elif pTbin == 5: cut = 0.00804545454545*pT + -1.20235454545
  else: cut = -0.8469 

  pass_pliv_verytight = pliv < cut
  ```

## barrel region electron working point
  pT refers to lepton pT in GeV, pTbin refers to "**PromptLeptonImprovedInput_MVAXBin**" and pliv refres to "**PromptLeptonImprovedVetoBARR**".
  - PLImprovedTight cut values:

  ![promptleptontight barr elec](plots/elec_barr/PLIVWP_cuts_cut.png)
  ```py
# lepton pT [GeV]
  cut = -99.
  if pTbin <= 0: cut = -0.7842
  elif pTbin == 1: cut = -0.00968*pT + -0.6874
  elif pTbin == 2: cut = -0.00154*pT + -0.8511
  elif pTbin == 3: cut = 0.0018*pT + -0.9416
  elif pTbin == 4: cut = 0.00958571428571*pT + -1.15654285714
  elif pTbin == 5: cut = 0.00811818181818*pT + -1.12238181818
  else: cut = -0.7039

  pass_pliv_tight = pliv < cut
  ```
  - PLImprovedVeryTight cut values:

  ```py
# lepton pT [GeV]
  cut = -99.
  if pTbin <= 0: cut = -0.7984
  elif pTbin == 1: cut = -0.01144*pT + -0.684
  elif pTbin == 2: cut = -0.00406*pT + -0.8388
  elif pTbin == 3: cut = -0.00022*pT + -0.9353
  elif pTbin == 4: cut = 0.000614285714286*pT + -0.971457142857
  elif pTbin == 5: cut = 0.00247272727273*pT + -1.04232727273
  else: cut = -0.9359

  pass_pliv_verytight = pliv < cut
  ```

## endcap region electron working point
  pT refers to lepton pT in GeV, pTbin refers to "**PromptLeptonImprovedInput_MVAXBin**" and pliv refres to "**PromptLeptonImprovedVetoECAP**".
  - PLImprovedTight cut values:

  ![promptleptontight endcap elec](plots/elec_endcap/PLIVWP_cuts_cut.png)
  ```py
# lepton pT [GeV]
  cut = -99.
  if pTbin <= 0: cut = -0.6885
  elif pTbin == 1: cut = -0.0011*pT + -0.6775
  elif pTbin == 2: cut = 0.0062*pT + -0.8402
  elif pTbin == 3: cut = -0.0034*pT + -0.6939
  elif pTbin == 4: cut = -0.00357142857143*pT + -0.716614285714
  elif pTbin == 5: cut = -0.00439090909091*pT + -0.719990909091
  else: cut = -0.914

  pass_pliv_tight = pliv < cut
  ```
  - PLImprovedVeryTight cut values:

  ```py
# lepton pT [GeV]
  cut = -99.
  if pTbin <= 0: cut = -0.8353
  elif pTbin == 1: cut = 0.00478*pT + -0.8831
  elif pTbin == 2: cut = 0.0055*pT + -0.9671
  elif pTbin == 3: cut = -0.00016*pT + -0.8955
  elif pTbin == 4: cut = -0.000257142857143*pT + -0.915871428571
  elif pTbin == 5: cut = -0.000736363636364*pT + -0.922836363636
  else: cut = -0.9698

  pass_pliv_verytight = pliv < cut
  ```


