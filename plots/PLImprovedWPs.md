# muon working point

| prompt muon efficiency |  non-prompt muon efficiency|
| ------ | ------ |
| ![prompt muon eff](plots/muon/PLIV_WP_compare_prompt_ptbin1000_eff_noiso.png)| ![non-prompt muon eff](plots/muon/PLIV_WP_compare_nonprompt_eff_noiso.png)|
| **cut values** | |
![cut value](plots/muon/PLIVWP_cuts_cut.png)

# barrel region electron working point
| prompt barrel region electron efficiency |  non-prompt barrel region electron efficiency|
| ------ | ------ |
| ![prompt elec_barr eff](plots/elec_barr/PLIV_WP_compare_prompt_ptbin1000_large_eff_noiso.png) | ![non-prompt elec_barr eff](plots/elec_barr/PLIV_WP_compare_nonprompt_eff_noiso.png) |
| **cut values** | |
![cut value](plots/elec_barr/PLIVWP_cuts_cut.png)

# endcap region electron working point
| prompt endcap region  electron efficiency |  non-prompt endcap region electron efficiency|
| ------ | ------ |
|![prompt elec_endcap eff](plots/elec_endcap/PLIV_WP_compare_prompt_ptbin1000_large_eff_noiso.png) |![non-prompt elec_endcap eff](plots/elec_endcap/PLIV_WP_compare_nonprompt_eff_noiso.png) |
| **cut values** | |
![cut value](plots/elec_endcap/PLIVWP_cuts_cut.png)

